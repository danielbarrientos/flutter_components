import 'package:flutter/material.dart';

class AlertPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
   
    return Scaffold(
      appBar: AppBar(
        title: Text('Alert Page'),
      ),
      body: Center(
        child: RaisedButton(
          child: Text('mostrar alerta'),
          color: Colors.blue,
          textColor: Colors.white,
          shape: StadiumBorder(),
          onPressed: ()=> _mostrarAlerta(context),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.keyboard_arrow_left),
        onPressed: (){
          Navigator.pop(context);
        },
      ),
    );
  }

  void _mostrarAlerta(context){
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) {
         return AlertDialog(
           shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(20.0)),
           title: Text('Título'),
           content: Column(
             mainAxisSize: MainAxisSize.min,
             children: [
               Text('este esm el conteniudo de la alerta'),
               FlutterLogo(
                 size: 100.0,
               )
             ],
           ),
           actions: [
             FlatButton(
               child: Text('cancelar'),
               onPressed: ()=> Navigator.of(context).pop(),
             ),
              FlatButton(
               child: Text('Ok'),
               onPressed: ()=> Navigator.of(context).pop(),
             )
           ],
         );
      },
    );
  }

  
}
